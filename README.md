# Freedesktop SDK Website

The Freedesktop SDK project is a free and open source project that provides Platform and SDK runtimes for Flatpak apps and runtimes based on Freedesktop modules. This is the project website source. It's written in markdown and uses [jekyll](https://jekyllrb.com/) to automatically build the static website with gitlab's CI.

## Setup

The process of setting up the site locally consists of:

- Install ruby [gem bundler](https://bundler.io/). On [Fedora](https://getfedora.org/)/in the [Toolbx](https://containertoolbx.org) you do:

```
toolbox enter
sudo dnf install rubygem-bundler
cd freedesktop-sdk-website
bundle install
```

- Test the site locally:
```
bundle exec jekyll s
```

Written with love using [Apostrophe](https://flathub.org/apps/details/org.gnome.gitlab.somas.Apostrophe).